package com.gerardwisniewski.fileserver;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;

class FileReceiverHandler {
    @SuppressWarnings("ResultOfMethodCallIgnored")
    static void receiveFile(Socket client, String dir) throws IOException {
        String name;
        long size;

        try (DataInputStream dataInputStream = new DataInputStream(client.getInputStream())) {
            try {
                name = dataInputStream.readUTF();
                size = dataInputStream.readLong();
            } catch (IOException e) {
                System.err.println("Failed to read the header, dropping client.");
                client.close();
                return;
            }

            File file = new File(dir, name);

            System.out.printf("Downloading %s from %s (size %d)\n", name, client.getInetAddress().toString(), size);

            file.createNewFile();

            try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
                int totalRead = 0;
                int read;
                byte[] buffer = new byte[4096];

                while ((read = dataInputStream.read(buffer)) != -1) {
                    totalRead += read;
                    fileOutputStream.write(buffer, 0, read);
                }

                if (totalRead!=size)
                    System.err.println("File size doesn't match");

                System.out.println("Finished downloading " + name);
            } catch (IOException e) {
                System.err.println("Critical error encountered while reading " + name +
                        ", error reported: " + e.getMessage());
                file.delete();
            }
        }
        client.close();
    }
}
