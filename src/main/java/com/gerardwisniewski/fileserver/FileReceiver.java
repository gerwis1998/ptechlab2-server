package com.gerardwisniewski.fileserver;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SuppressWarnings("ResultOfMethodCallIgnored")
class FileReceiver {

    private final String dir;
    private final int port;
    private final int threads;

    FileReceiver(int port, String dir, int threads) {

        this.dir = dir;
        new File(dir).mkdirs();
        this.port = port;
        this.threads = threads;
    }

    void startReceiving() {
        System.out.printf("Starting at port %d\n", port);
        ExecutorService executor = Executors.newFixedThreadPool(threads);

        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (true) {
                try {
                    final Socket socket = serverSocket.accept();
                    executor.submit(() -> {
                        try {
                            FileReceiverHandler.receiveFile(socket, dir);
                        } catch (IOException e) {
                            System.err.println("Problem during receiving: " + e.getMessage());
                        }
                    });
                } catch (IOException e) {
                    System.err.println("Encountered a problem while accepting new clients");
                    System.exit(1);
                }
            }
        } catch (IOException e) {
            System.err.println("Fatal Error");
            System.exit(1);
        }
    }
}
