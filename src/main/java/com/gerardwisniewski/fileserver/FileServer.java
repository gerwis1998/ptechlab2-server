package com.gerardwisniewski.fileserver;


import org.apache.commons.cli.*;

class FileServer {

    @SuppressWarnings("deprecation")
    public static void main(String[] args)  {

        int port = 5000;
        String dir = ".";
        int threads = 8;

        Options options = new Options();

        Option portOpt = Option.builder("p").hasArg().build();
        portOpt.setType(Number.class);

        Option dirOpt = Option.builder("d").hasArg().build();
        dirOpt.setType(String.class);

        Option threadsOpt = Option.builder("t").hasArg().build();
        threadsOpt.setType(Number.class);

        options.addOption(portOpt);
        options.addOption(dirOpt);
        options.addOption(threadsOpt);

        CommandLineParser cmdLineParser = new PosixParser();

        try {
            CommandLine cmdLine = cmdLineParser.parse(options, args);
            if (cmdLine.hasOption("p")) {
                port = ((Number)cmdLine.getParsedOptionValue("p")).intValue();
            }
            if (cmdLine.hasOption("d")) {
                dir = (String)cmdLine.getParsedOptionValue("d");
            }
            if (cmdLine.hasOption("t")) {
                threads = ((Number)cmdLine.getParsedOptionValue("t")).intValue();
            }
        } catch (ParseException e) {
            System.err.println("Invalid arguments");
        }

        FileReceiver fileReceiver = new FileReceiver(port, dir, threads);
        fileReceiver.startReceiving();
    }
}
